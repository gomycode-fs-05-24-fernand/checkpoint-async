const data = [1,2,3,4,5]

async function iterateWithAsyncAwait(data){
    for( const element of data){
        await new Promise(resolve => setTimeout(resolve,1000));
        console.log(element);
    }
}



async function awaitCall(){

    try {
        await fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => response.json())
        .then(json => console.log(json))
        .catch()
    }catch(error){
        console.log(error);
    }
    
}

async function one(){
    await new Promise(resolve => setTimeout(resolve,1000));
    console.log('one')
}


async function two(){
    await new Promise(resolve => setTimeout(resolve,1000));
    console.log('two')
}


async function three(){
    await new Promise(resolve => setTimeout(resolve,1000));
    console.log('three')
}




async function chainedAsyncFunction(){
    await one()
    await two()
    await three()
}

//chainedAsyncFunction()

async function concurrentRequest(){

    const call01 = new Promise((resolve) => {
            setTimeout(() => {
                resolve('api 1 result');
            }, 1000);
        })

    const call02 = new Promise((resolve) => {
            setTimeout(() => {
                resolve('api 2 result');
            }, 1000);
        })


    Promise.all([call01,call02])
        .then((result) => console.log(result));

}

//concurrentRequest();


const urls = ['https://jsonplaceholder.typicode.com/todos/1', 'https://jsonplaceholder.typicode.com/todos/2', 'https://jsonplaceholder.typicode.com/todos/3']

async function parallelCalls(urls){

    const fetchPromises = urls.map(url => fetch(url).then(response => response.json()));
    Promise.all(fetchPromises)
        .then((result) => console.log(result));
}


parallelCalls(urls);